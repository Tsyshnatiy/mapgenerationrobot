#include <Servo.h>
#include <Wire.h>

#define SLAVE_ADDRESS 0x06

Servo servoMain; // Обьект Servo
int servoPin = 9;
int offset = 0;
int zero = 90;
int servoLimit = 60;
int servoSpeed = 0;
int engineLFW = 11;
int engineLBK = 3;
int engineRFW = 5;
int engineRBK = 6;

int state = 0;

byte command[2];

void powerOff()
{
  analogWrite(engineLFW, 0);
  analogWrite(engineLBK, 0);
  analogWrite(engineRFW, 0);
  analogWrite(engineRBK, 0);
}

void left()
{
  powerOff();
  //Serial.println("LEFT");
  analogWrite(engineLBK, 128);
  analogWrite(engineRFW, 128);
}

void right()
{
  powerOff();
  //Serial.println("RIGHT");
  analogWrite(engineLFW, 128);
  analogWrite(engineRBK, 128);
}

void forward()
{
  powerOff();
  //Serial.println("FORWARD");
  analogWrite(engineLFW, 96);
  analogWrite(engineRFW, 96);
}

void backward()
{
  powerOff();
  //Serial.println("BACKWARD");
  analogWrite(engineLBK, 96);
  analogWrite(engineRBK, 96);
}

void stopEngines()
{
  powerOff();
  //Serial.println("STOP");
}

void servoWrite(int spd)
{
  int servoPos = servoMain.read();
  if (abs(servoPos + spd) > zero + offset + servoLimit)
  {
    spd = 0;
  }
  
  servoMain.write(servoPos + spd);
}

void setup() 
{
  pinMode(engineLFW, OUTPUT);
  pinMode(engineLBK, OUTPUT);
  pinMode(engineRFW, OUTPUT);
  pinMode(engineRBK, OUTPUT);
  
  Serial.begin(9600);
  // initialize i2c as slave
  Wire.begin(SLAVE_ADDRESS);
  servoMain.attach(servoPin);
  command[0] = 0;
  command[1] = 0;
  // define callbacks for i2c communication
  Wire.onReceive(receiveData);
  servoMain.write(zero + offset);
  delay(1000);
}
 
void loop() 
{
  int curState = command[0];
 
  if (state != curState)
  {
    switch(command[0])
    {
      case 0:
        stopEngines();
        break;
      case 1:
        forward();
        break;
      case 2:
        backward();
        break;
      case 3:
        left();
        break;
      case 4:
        right();
        break;
      default:
        break;
    }
  }

  if (command[0] == 5)
  {
    servoSpeed = -1 * command[1];
  }
  else if (command[0] == 6)
  {
    servoSpeed = command[1];
  }
  
  state = curState;
  servoWrite(servoSpeed);
  delay(50);
}
 
// callback for received data
void receiveData(int byteCount)
{
  int i = 0;
  byte cmd[2];
  while(Wire.available()) 
  {
    cmd[i++] = Wire.read();
  }
  command[0] = cmd[0];
  command[1] = cmd[1];
}
