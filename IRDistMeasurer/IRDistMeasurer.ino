int val;
float distance = 0;

void setup()
{
  Serial.begin(9600);
}

void loop()
{
  val = analogRead(0); 
  
  distance = 
  1.46023717187441 
  + 2388.07395713086 / val 
  - 32995.4585565879 / val / val;
  
  Serial.println(distance);
  // wait 10ms for ADC to reset before next reading:
  delay(100);                 
}
