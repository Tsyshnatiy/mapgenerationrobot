#include <calibration.h>
#include <distanceCtx.h>

#include <TimerOne.h>
#include <Encoder.h>

#define PIN_ENCODER_CLK 10
#define PIN_ENCODER_DT 9

Encoder encoder(PIN_ENCODER_DT, PIN_ENCODER_CLK);
float rotaryDist = 0;
float hallDistance = 0;
distanceContext* dctx;

void showDistance()
{
  Serial.print("Rotary distance: ");
  Serial.println(rotaryDist);
  Serial.print("Hall distance: ");
  Serial.println(hallDistance);
}

void setup() 
{   
	Serial.begin(9600); 
        // set timer to 1 sec
        Timer1.initialize(1000000);
        Timer1.attachInterrupt(showDistance); // attach the service routine here
        
        dctx = createDistCtx();
        dctx->prev = sample2Degrees(analogRead(0));
        delay(10);
        dctx->cur = sample2Degrees(analogRead(0));
        delay(10);
}

float computeDistByRotary(Encoder& enc)
{
	static long oldPosition = 0;
	
	static const float wheelDiam = 3.0f; // cm
	// 17 rotary encoder ticks equal to one round of wheel
	static const int rotaryProportion = 17;
        // how many ticks encoder totaly made
	static int ticksCntr = 0;

	long newPosition = enc.read() / 4;
	
	// handle both rotation directions at once
	// assume rotation only in one side during experiment
	if (newPosition > oldPosition || newPosition < oldPosition) 
	{       
		ticksCntr++;
		oldPosition = newPosition;
	}
	
	return (1.0f * ticksCntr / rotaryProportion) * 3.141592 * wheelDiam;
}

void loop() 
{
        //rotaryDist = computeDistByRotary(encoder);
        
        dctx->future = sample2Degrees(analogRead(0));
        hallDistance = updateDistance(dctx);
        
	delay(10); //задержка 5 мс
}
