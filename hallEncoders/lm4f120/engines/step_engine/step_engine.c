#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"

#include "step_engine.h"

inline static unsigned long int period_2_delay(double step_period)
{
	// 3 tacts for 1 delay, so divide by 3.
	// SysCtlClockGet() / 3 represents one second delay.
	return SysCtlClockGet() / 3 * step_period;
}

int create_default_turm_step_engine(step_engine* s_eng)
{
	s_eng->cur_angle = 0;
	s_eng->direction = LEFT;
	s_eng->position = 0;

	s_eng->pin0 = GPIO_PIN_4;
	s_eng->pin1 = GPIO_PIN_5;
	s_eng->pin2 = GPIO_PIN_6;
	s_eng->pin3 = GPIO_PIN_7;
	s_eng->pins_all = s_eng->pin0 | s_eng->pin1 | s_eng->pin2 | s_eng->pin3;

	s_eng->port = SYSCTL_PERIPH_GPIOC;
	s_eng->port_base = GPIO_PORTC_BASE;

    SysCtlPeripheralEnable(s_eng->port);
    SysCtlDelay(2);
    GPIOPinTypeGPIOOutput(s_eng->port_base, s_eng->pins_all);

	// Step C0 C1 C2 C3
	// 1	 1  1  0  0
	// 2     0  1  1  0
	// 3     0  0  1  1
	// 4     1  0  0  1
	s_eng->step0 = s_eng->pin0 | s_eng->pin1;
	s_eng->step1 = s_eng->pin1 | s_eng->pin2;
	s_eng->step2 = s_eng->pin2 | s_eng->pin3;
	s_eng->step3 = s_eng->pin3 | s_eng->pin0;

	s_eng->step_size = 5.625f / 64; // 28BYJ-48-5V
	s_eng->calc_angle = 0;
	s_eng->phase_count = 4;
	s_eng->angle_speed = 0;

	return 0;
}

int set_speed(step_engine* s_eng, double angle_speed)
{
	double step_period = 0;
	if (angle_speed <= 0)
	{
		return -1;
	}

	s_eng->angle_speed = angle_speed;

	// [deg/sec] / [deg] = [1 / sec] => [deg] / [deg / sec] = [sec]
	step_period = s_eng->step_size / s_eng->angle_speed;
	s_eng->delay_btw_steps = period_2_delay(step_period);

	return 0;
}

int set_direction(step_engine* s_eng, step_engine_direction new_dir)
{
	switch(new_dir)
	{
	case LEFT:
	case RIGHT:
		s_eng->direction = new_dir;
		break;
	default:
		// report error if new_dir is invalid
		return -1;
	}

	return 0;
}

int step(step_engine* s_eng)
{
	if (s_eng->angle_speed <= 0)
	{
		return -1; // return on non-valid angle speed
	}

	switch(s_eng->position)
	{
		case 0:
			GPIOPinWrite(GPIO_PORTC_BASE, s_eng->pins_all, s_eng->step0);
			break;
		case 1:
			GPIOPinWrite(GPIO_PORTC_BASE, s_eng->pins_all, s_eng->step1);
			break;
		case 2:
			GPIOPinWrite(GPIO_PORTC_BASE, s_eng->pins_all, s_eng->step2);
			break;
		case 3:
			GPIOPinWrite(GPIO_PORTC_BASE, s_eng->pins_all, s_eng->step3);
			break;
		default:
			break;
	}

	s_eng->position = (s_eng->direction ? s_eng->position - 1 : s_eng->position + 1) % s_eng->phase_count;

	if (s_eng->calc_angle)
	{
		s_eng->cur_angle += s_eng->calc_angle(1); // only one step was done
	}
	return 0;
}
