/*
 * step_engine.h
 *
 *  Created on: 29 ���� 2015 �.
 *      Author: Vladimir
 */

#ifndef STEP_ENGINE_H_
#define STEP_ENGINE_H_

#include <stdint.h>

// takes steps count, returns computed angle
typedef double (*calc_angle_func)(int);

typedef enum step_engine_direction_t
{
	LEFT,
	RIGHT
}step_engine_direction;

typedef struct step_engine_t
{
	double step_size; // in degrees
	double cur_angle;
	double angle_speed;	// in degrees per second
	unsigned long int delay_btw_steps;
	step_engine_direction direction;
	char position;
	char phase_count;
	calc_angle_func calc_angle;

	unsigned long port;
	unsigned long port_base;

	unsigned int pin0;
	unsigned int pin1;
	unsigned int pin2;
	unsigned int pin3;
	unsigned int pins_all;

	unsigned int step0;
	unsigned int step1;
	unsigned int step2;
	unsigned int step3;
} step_engine;

// 28BYJ-48-5V engine
int create_default_turm_step_engine(step_engine* s_eng);

// Sets angle speed for step engine.
// Returns 0 on success
// Returns -1 if angle_speed is not valid
int set_speed(step_engine* s_eng, double angle_speed);

// Sets rotation direction
// Returns 0 on success
// Returns -1
int set_direction(step_engine* s_eng, step_engine_direction new_dir);

// Returns 0 on success
// Returns -1 if angle_speed is not set
int step(step_engine* s_eng);

#endif /* STEP_ENGINE_H_ */
