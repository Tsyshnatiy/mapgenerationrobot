#ifndef HALL_DIST_CTX_H
#define HALL_DIST_CTX_H

#include <math.h>

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

#define NOT_DEFINED 1000000
#define WHEEL_DIAM 5

static const short ext_threshold = 15;
static const short mid_pt_threshold = 5;
static const float deg2cm = 1.0f / 360 * WHEEL_DIAM * M_PI;

typedef struct distanceContext_t
{
	float prev;
	float cur; // algorithm implements one sample delay
	float future;
	float lastExt;
	float lastMeasurePt;
	float distance;
} distanceContext;

distanceContext* createDistCtx();

float updateDistance(distanceContext* ctx);

void resetDistCtx(distanceContext* ctx);

void destroyDistCtx(distanceContext* ctx);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif //HALL_DIST_CTX_H