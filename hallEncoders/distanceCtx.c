#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "distanceCtx.h"

distanceContext* createDistCtx()
{
	distanceContext* res = (distanceContext*) malloc(sizeof(distanceContext));
	res->lastExt = NOT_DEFINED;
	res->lastMeasurePt = NOT_DEFINED;
	res->prev = 0;
	res->cur = 0;
	res->future = 0;
	res->distance = 0;
	return res;
}

void resetDistCtx(distanceContext* ctx)
{
	ctx->lastExt = NOT_DEFINED;
	ctx->lastMeasurePt = NOT_DEFINED;
	ctx->prev = 0;
	ctx->cur = 0;
	ctx->future = 0;
	ctx->distance = 0;
}

float updateDistance(distanceContext* ctx)
{
	float result = 0;
	// if not defined then set it be left border
	if (ctx->lastExt == NOT_DEFINED)
	{
		ctx->lastExt = ctx->prev;
		ctx->lastMeasurePt = ctx->prev;
		return result;
	}
	
	if ((ctx->cur >= ctx->prev && ctx->cur > ctx->future 
		|| ctx->cur > ctx->prev && ctx->cur >= ctx->future
		|| ctx->cur <= ctx->prev && ctx->cur < ctx->future 
		|| ctx->cur < ctx->prev && ctx->cur <= ctx->future)
		&& fabs(ctx->cur - ctx->lastExt) > ext_threshold)
	{
		ctx->distance += fabs(ctx->cur - ctx->lastMeasurePt) * deg2cm;
		ctx->lastMeasurePt = ctx->cur;
		ctx->lastExt = ctx->cur;
	}
	else if (fabs(ctx->cur - ctx->lastMeasurePt) > mid_pt_threshold)
	{
		ctx->distance += fabs(ctx->cur - ctx->lastMeasurePt) * deg2cm;
		ctx->lastMeasurePt = ctx->cur;
	}
	
	ctx->prev = ctx->cur;
	ctx->cur = ctx->future;
	return ctx->distance;
}

void destroyDistCtx(distanceContext* ctx)
{
	if (ctx)
	{
		free(ctx);
		ctx = NULL;
	}
}