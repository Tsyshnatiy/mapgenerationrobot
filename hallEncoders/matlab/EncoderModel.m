clear all;
fileID = fopen('hallData.txt','r');
formatSpec = '%f';
data = fscanf(fileID, formatSpec);
t =(0:(size(data,1) - 1));
%calibration
ang2pi = max(data);
ang0 = min(data);
calibAngPi = ang2pi;
calibAng0 = ang0;

% Line eq y = 360 / (c2 - c1) * x + 360 * c1 / (c1 - c2);
% c1 = calibAng0; c2 = calibAngPi
degrees = data .* (180 / (calibAngPi - calibAng0)) + 180 * calibAng0 / (calibAng0 - calibAngPi);
figure;
plot(t, data, t, degrees);

wheelDiameter = 5;
extremumEpsDeg = 5;
ptEpsDeg = 5;
N = length(degrees);

lastExtremum = degrees(1);
lastPt = degrees(1);

hold on;
plot(0, degrees(1), 'r*');
hold off;
diff = 0;
dist = zeros(N,1);
passedDistance = 0;
for i = 2:N-1
    if (abs(degrees(i) - lastExtremum) >= extremumEpsDeg ...
        && ((degrees(i) >= degrees(i - 1) && degrees(i) > degrees(i + 1)) ...
        || (degrees(i) > degrees(i - 1) && degrees(i) >= degrees(i + 1)) ...
        || (degrees(i) <= degrees(i - 1) && degrees(i) < degrees(i + 1)) ...
        || (degrees(i) < degrees(i - 1) && degrees(i) <= degrees(i + 1))))
    
        localExtremum = degrees(i);
        hold on;
        plot(i-1, degrees(i), 'r*');
        hold off;
        passedDistance = passedDistance + abs(lastExtremum - localExtremum) * (1 / 360 * pi * wheelDiameter);
        lastExtremum = localExtremum;
    elseif (abs(degrees(i) - lastPt) >= ptEpsDeg)
        hold on;
        plot(i-1, degrees(i), 'b*');
        hold off;
        d = abs(lastExtremum - degrees(i)) * (1 / 360 * pi * wheelDiameter) + passedDistance;
        disp(d);
        lastPt = degrees(i);
    end
end

disp(passedDistance);