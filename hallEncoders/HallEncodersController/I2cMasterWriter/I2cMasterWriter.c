#define F_CPU 8000000UL

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "../../HallEncodersController/HallEncodersController/utility/i2c/i2c_master.h"

volatile uint8_t dir = 0x00;
volatile uint8_t spd = 40;

// changes stepper direction
ISR (INT0_vect)
{
	I2C_master_start(0x06);
	I2C_master_write(0x01);
	I2C_master_stop();
	
	I2C_master_start(0x06);
	I2C_master_write(dir++ % 2);
	I2C_master_stop();
	
	I2C_master_start(0x06);
	I2C_master_write(0x02);
	I2C_master_stop();
	
	I2C_master_start(0x06);
	I2C_master_write(spd);
	I2C_master_stop();
}

void forward()
{
		I2C_master_start(0x06);
		I2C_master_write(0x03);
		I2C_master_stop();
		
		I2C_master_start(0x06);
		I2C_master_write(spd);
		I2C_master_stop();
		
		I2C_master_start(0x06);
		I2C_master_write(0x04);
		I2C_master_stop();
		
		I2C_master_start(0x06);
		I2C_master_write(spd);
		I2C_master_stop();
}

void backward()
{
	I2C_master_start(0x06);
	I2C_master_write(0x05);
	I2C_master_stop();
	
	I2C_master_start(0x06);
	I2C_master_write(spd);
	I2C_master_stop();
	
	I2C_master_start(0x06);
	I2C_master_write(0x06);
	I2C_master_stop();
	
	I2C_master_start(0x06);
	I2C_master_write(spd);
	I2C_master_stop();
}

ISR (INT1_vect)
{
	static int triggerCntr = 0;
	if (triggerCntr++ % 2)
	{
		backward();
	}
	else
	{
		forward();
	}
}

int main(void)
{
	DDRB |= (1 << DDB1);
	
	// Configure external interrupt from button on DDD2
    DDRD &= ~(1 << DDD2);
    PORTD |= (1 << PORTD2);
	EICRA |= (1 << ISC01) | (1 << ISC00);	//Trigger on rising edge of INT0
    EIMSK |= (1 << INT0);					//Enable INT0
	
	// Configure external interrupt from button on DDD3
	DDRD &= ~(1 << DDD3);
	PORTD |= (1 << PORTD3);
	EICRA |= (1 << ISC01) | (1 << ISC00);	//Trigger on rising edge of INT0
	EIMSK |= (1 << INT1);					//Enable INT1
	
	I2C_master_init();
	_delay_ms(200);
	
	sei();

	while(1)
	{
	}
}