#define F_CPU 8000000UL

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include "utility/i2c/i2c_slave.h"

#define DEMO

#define I2C_ADDRESS 0x06
#define SPD2PWM(spd) (uint8_t)(spd) * (255.0f / 100)
//	command				code	arg
//	turm rotation dir	0x01	0-100	(1 byte)
//	turm speed			0x02	0-100	(1 byte)
//	left eng speed		0x03	0-100	(1 byte)
//	right eng speed		0x04	0-100	(1 byte)

#define TURM_ROT_DIR		0x01
#define TURM_SPD			0x02
#define LEFT_ENG_SPD_FW		0x03
#define RIGHT_ENG_SPD_FW	0x04
#define LEFT_ENG_SPD_BK		0x05
#define RIGHT_ENG_SPD_BK	0x06

static volatile uint8_t command_state = 0;	// 0 - empty, 1 - command code received
static volatile uint8_t command_code = 0;

static volatile uint8_t turm_rot_direction = 0;	// 0 or 1
static volatile uint8_t turm_speed		= 0;	// 0 - 100
static volatile uint8_t leng_speed_fw	= 0;	// 0 - 100
static volatile uint8_t reng_speed_fw	= 0;	// 0 - 100
static volatile uint8_t leng_speed_bk	= 0;	// 0 - 100
static volatile uint8_t reng_speed_bk	= 0;	// 0 - 100

void on_i2c_byte_received(uint8_t byte)
{
	if (!command_state)
	{
		switch(byte)
		{
			case TURM_ROT_DIR:
			case TURM_SPD:
			case LEFT_ENG_SPD_FW:
			case RIGHT_ENG_SPD_FW:
			case LEFT_ENG_SPD_BK:
			case RIGHT_ENG_SPD_BK:
				command_code = byte;
				break;
			default:
				break; // just ignore unsupported cmds
		}
		command_state = 1;
	}
	else
	{
		switch(command_code)
		{
			case TURM_ROT_DIR:
				turm_rot_direction = byte;
				break;
			case TURM_SPD:
				turm_speed = byte;
				break;
				
			case LEFT_ENG_SPD_FW:
				leng_speed_fw = SPD2PWM(byte);
				leng_speed_bk = 0;
				
				TCCR1A |= (1 << COM1A1);
				TCCR1A &= ~(1 << COM1B1);
				
				OCR1B = 0;		// disable backward
				OCR1A = leng_speed_fw;	// enable forward
				break;
			case RIGHT_ENG_SPD_FW:
				reng_speed_bk = 0;
				reng_speed_fw = SPD2PWM(byte);
				
				TCCR2A |= (1 << COM2A1);
				TCCR2A &= ~(1 << COM2B1);
				
				OCR2B = 0;		// disable backward
				OCR2A = reng_speed_fw;	// enable forward
				break;
				
			case LEFT_ENG_SPD_BK:
				leng_speed_bk = SPD2PWM(byte);
				leng_speed_fw = 0;
				
				TCCR1A |= (1 << COM1B1);
				TCCR1A &= ~(1 << COM1A1);
				
				OCR1A = 0;		// disable forward
				OCR1B = leng_speed_bk;	// enable backward
				break;
			case RIGHT_ENG_SPD_BK:
				reng_speed_bk = SPD2PWM(byte);
				reng_speed_fw = 0;
				
				TCCR2A |= (1 << COM2B1);
				TCCR2A &= ~(1 << COM2A1);
				
 				OCR2A = 0;		// disable forward
				OCR2B = reng_speed_bk;	// enable backward
				break;
				
			default:
				break; // just ignore unsupported cmds
		}
		// command executed, read next command
		command_state = 0;
	}
}

void pwm_initialize() 
{
	// init timers
	TCCR1A |= (1 << WGM10); // (1 << COM1A1) | (1 << COM1B1) |
	TCCR1B |= (1 << WGM12)  | (1 << CS11)	| (1 << CS10);
	TCCR2A |= (1 << WGM21) | (1 << WGM20); // (1 << COM2A1) | (1 << COM2B1) |
	TCCR2B |= (1 << CS22);

	OCR1A = 0;
	OCR1B = 0;
	OCR2A = 0;
	OCR2B = 0;
	
	// 4 PWM channel outputs
	DDRB |= (1 << PB1); // OC1A
	DDRB |= (1 << PB2); // OC1B
	
	DDRB |= (1 << PB3); // OC2A
	DDRD |= (1 << PD3); // OC2B
}

int main(void)
{
	static char turm_state = 0;
	int i = 0;
	cli();
	
	pwm_initialize();
	
	// initialize stepper
	DDRC |= (1 << DDC0) | (1 << DDC1) | (1 << DDC2) | (1 << DDC3);
	
	// init i2c indicator
	DDRB |= (1 << DDB0);
	PORTB = 0x00;
	
	// init i2c bus
	i2c_slave_init(I2C_ADDRESS); // initialize as slave with address 0x32
	i2c_on_byte_recv = on_i2c_byte_received;
	sei();

	for(;;)
	{
		if (!turm_speed)
		{
			continue;
		}
		
		switch(turm_state)
		{
			#ifdef DEMO
			case 0:
				PORTC = (1 << DDC0) | (1 << DDC1);
				break;
			case 1:
				PORTC = (1 << DDC1) | (1 << DDC2);
				break;
			case 2:
				PORTC = (1 << DDC2) | (1 << DDC3);
				break;
			case 3:
				PORTC = (1 << DDC3) | (1 << DDC0);
				break;
			#else
			// Step C0 C1 C2 C3
			// 1	   1  0  1  0
			// 2     0  1  1  0
			// 3     0  1  0  1
			// 4     1  0  0  1
			case 0:
				PORTC = (1 << DDC0) | (1 << DDC2);
				break;
			case 1:
				PORTC = (1 << DDC1) | (1 << DDC2);
				break;
			case 2:
				PORTC = (1 << DDC1) | (1 << DDC3);
				break;
			case 3:
				PORTC = (1 << DDC0) | (1 << DDC3);
				break;
			#endif // DEMO
			default:
				break;
		}
		
		turm_state = turm_rot_direction ? turm_state - 1 : turm_state + 1;
		turm_state = turm_state % 4;
		for (i = 0 ; i < 100 - turm_speed + 200 ; ++i)
		{
			_delay_ms(1);
		}
	}

	return 0;
}