#ifndef I2C_MASTER_H
#define I2C_MASTER_H

#define I2C_READ 0x01
#define I2C_WRITE 0x00

void I2C_master_init(void);
uint8_t I2C_master_start(uint8_t address);
uint8_t I2C_master_write(uint8_t data);
uint8_t I2C_master_read_ack(void);
uint8_t I2C_master_read_nack(void);
void I2C_master_stop(void);

#endif // I2C_MASTER_H