#ifndef HALL_CALIB_H
#define HALL_CALIB_H

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

#define CALIB_ANG_0 260
#define CALIB_ANG_PI 777
#define WORKING_RANGE (CALIB_ANG_PI - CALIB_ANG_0)

float sample2Degrees(int sample);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif //HALL_CALIB_H