#include "calibration.h"

float sample2Degrees(int sample)
{
	return sample * (180.0f / WORKING_RANGE) + 180.0f * CALIB_ANG_0 / WORKING_RANGE;
}