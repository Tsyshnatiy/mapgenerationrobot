#include <stdio.h>
#include "calibration.h"
#include "distanceCtx.h"

int main() 
{
	FILE *dataFd = fopen("TestData/hallData.txt", "r");
	distanceContext* dctx = createDistCtx();
	
	// init, read first 2 values
	fscanf(dataFd, "%f", &(dctx->prev));
	fscanf(dataFd, "%f", &(dctx->cur));
	dctx->prev = sample2Degrees(dctx->prev);
	dctx->cur = sample2Degrees(dctx->cur);
	printf("%f\n", dctx->prev);
	printf("%f\n", dctx->cur);
	while (!feof(dataFd))
	{
		fscanf(dataFd, "%f", &(dctx->future));
		dctx->future = sample2Degrees(dctx->future);
		
		updateDistance(dctx);
	}
	
	fclose(dataFd);
	printf("%f\n", dctx->distance);
	destroyDistCtx(dctx);

	return 0;
}