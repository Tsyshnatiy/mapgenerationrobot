int engineLFW = 11;
int engineLBK = 3;
int engineRFW = 5;
int engineRBK = 6;

void powerOff()
{
  analogWrite(engineLFW, 0);
  analogWrite(engineLBK, 0);
  analogWrite(engineRBK, 0);
  analogWrite(engineRFW, 0);
}

void left()
{
  analogWrite(engineLBK, 96);
  analogWrite(engineRFW, 96);
}

void forward()
{
  analogWrite(engineLFW, 160);
  analogWrite(engineRFW, 160);
}

void backward()
{
  Serial.println("BACKWARD");
  analogWrite(engineLBK, 96);
  analogWrite(engineRBK, 96);
}

void setup()
{
  pinMode(engineLFW, OUTPUT);
  pinMode(engineLBK, OUTPUT);
  pinMode(engineRFW, OUTPUT);
  pinMode(engineRBK, OUTPUT);
  powerOff();
  //backward();
  forward();
}

void loop()
{
  //left();
}
