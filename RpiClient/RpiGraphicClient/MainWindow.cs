﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;
using RpiGraphicClient.EventArgs;
using RpiGraphicClient.Utils;
using RpiGraphicClient.Control;

namespace RpiGraphicClient
{
    public partial class MainWindow : Form
    {

        /// <summary>
        /// Represent joistick control.
        /// </summary>
        private JoistickControl joistick;
        /// <summary>
        /// 
        /// </summary>
        private NetworkModule module;

        public MainWindow()
        {
            InitializeComponent();
            Logger.InitLogger();

            module = new NetworkModule("192.168.42.1", 5051); //192.168.42.1 10.0.0.12
            module.OnIncomingHeaders += HeadersHandler;
            module.Connect();

            var initialHeaders = new Dictionary<string, string>
                {
                    {HeaderNames.Manual, HeaderValues.True}
                };

            Task.Run(() =>
                {
                    try
                    {
                        module.StartIncomingPump();
                    }
                    catch (Exception ex)
                    {
                        Logger.Log("Got exception: " + ex.Message);
                    }
                });

            module.WriteHeaders(initialHeaders);
            
            // Create joistick control
            joistick = new JoistickControl();
            joistick.JoistickForwardValueChangedEvent += joistick_JoistickForwardValueChangedEvent;
            joistick.JoistickBackwardValueChangedEvent += joistick_JoistickBackwardValueChangedEvent;
            joistick.JoistickLeftValueChangedEvent += joistick_JoistickLeftValueChangedEvent;
            joistick.JoistickRigthValueChangedEvent += joistick_JoistickRigthValueChangedEvent;
        }
        /// <summary>
        /// Occuired when form is closed.
        /// </summary>
        /// <param name="sender">Link to sender who generate event.</param>
        /// <param name="e">Parameters of occuired event.</param>
        private void MainWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            var headers = new Dictionary<string, string>
                {
                    {HeaderNames.Exit, "now"}
                };
            module.WriteHeaders(headers);
            // Dispose all created objects
            this.joistick.Dispose();
            module.Disconnect();
            module.Dispose();
            Logger.Log("Exit");
            Application.Exit();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void HeadersHandler(object sender, IncomingHeadersEventArgs args)
        {
            Logger.LogHeaders(args.Headers);
        }

        /// <summary>
        /// Occuired when user press "Go_to_left".
        /// </summary>
        /// <param name="sender">Link to sender who generate event.</param>
        /// <param name="e">Parameters of occuired event.</param>
        void joistick_JoistickLeftValueChangedEvent(object sender, JoistickEventArgs e)
        {
            var headers = new Dictionary<string, string>
                {
                    { HeaderNames.Left, e.Value.ToString() }
                };
            module.WriteHeaders(headers);
            Logger.Log("Left: " + e.Value.ToString());
        }
        /// <summary>
        /// Occuired when user press "Go_to_rigth".
        /// </summary>
        /// <param name="sender">Link to sender who generate event.</param>
        /// <param name="e">Parameters of occuired event.</param>
        void joistick_JoistickRigthValueChangedEvent(object sender, JoistickEventArgs e)
        {
            var headers = new Dictionary<string, string>
                {
                    { HeaderNames.Right, e.Value.ToString() }
                };
            module.WriteHeaders(headers);
            Logger.Log("Right: " + e.Value.ToString());
        }
        /// <summary>
        /// Occuired when user press "Go_to_forward".
        /// </summary>
        /// <param name="sender">Link to sender who generate event.</param>
        /// <param name="e">Parameters of occuired event.</param>
        void joistick_JoistickForwardValueChangedEvent(object sender, JoistickEventArgs e)
        {
            var headers = new Dictionary<string, string>
                {
                    { HeaderNames.Forward, e.Value.ToString() }
                };
            module.WriteHeaders(headers);
            Logger.Log("Forward: " + e.Value.ToString());
        }
        /// <summary>
        /// Occuired when user press "Go_to_backward".
        /// </summary>
        /// <param name="sender">Link to sender who generate event.</param>
        /// <param name="e">Parameters of occuired event.</param>
        void joistick_JoistickBackwardValueChangedEvent(object sender, JoistickEventArgs e)
        {
            var headers = new Dictionary<string, string>
                {
                    { HeaderNames.Backward, e.Value.ToString() }
                };
            module.WriteHeaders(headers);
            Logger.Log("Backward: " + e.Value.ToString());
        }

        /// <summary>
        /// Occuired when user press button Exit.
        /// </summary>
        /// <param name="sender">Link to sender who generate event.</param>
        /// <param name="e">Parameters of occuired event.</param>
        private void ExitBtn_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }

    }
}
