﻿namespace RpiGraphicClient
{
    //Note: all headers are lowercased
    public static class HeaderNames
    {
        public const string Forward = "forward";
        public const string Backward = "backward";
        public const string Left = "left";
        public const string Right = "right";
        public const string Exit = "exit";
        public const string Id = "id";
        public const string Status = "status";
        public const string Manual = "manual";
        public const string Speed = "speed";
    }

    public static class HeaderValues
    {
        public const string True = "true";
        public const string False = "false";
    }

    public static class StatusCode
    {
        public const int Ok200 = 200;
        public const int InternalError500 = 500;
    }
}
