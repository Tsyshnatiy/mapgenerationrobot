﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.DirectInput;
using System.Windows.Forms;
using System.Threading;

namespace RpiGraphicClient.Control
{

    delegate void JoistickValueDelegate(object sender, JoistickEventArgs e);

    /// <summary>
    /// Represent connected joistick control.
    /// </summary>
    class JoistickControl : IDisposable
    {

        /// <summary>
        /// Dispose all unmanaged rescourses.
        /// </summary>
        public void Dispose()
        {
            this.isNeedStop = true;
            this.joist = null;
            this.di = null;
        }

        /// <summary>
        /// Zero point in joistick value.
        /// </summary>
        private const int ZeroPoint = 32767;
        /// <summary>
        /// Max value point in joistick.
        /// </summary>
        private const int MaxValuePoint = 65535;

        /// <summary>
        /// Represent DirectINput devise.
        /// </summary>
        private DirectInput di = new DirectInput();
        /// <summary>
        /// Represent the joistick as a device.
        /// </summary>
        private Joystick joist = null;
        /// <summary>
        /// Need to stop thread.
        /// </summary>
        private bool isNeedStop = false;
        /// <summary>
        /// Temp rotation angle value to the left (from 0 to 100).
        /// </summary>
        private byte tLeft = 0;
        /// <summary>
        /// Temp rotation angle value to the rigth (from 0 to 100).
        /// </summary>
        private byte tRigth = 0;
        /// <summary>
        /// Temp forward value (from 0 to 100).
        /// </summary>
        private byte tForward = 0;
        /// <summary>
        /// Temp backward value (from 0 to 100).
        /// </summary>
        private byte tBackward = 0;

        /// <summary>
        /// Return rotation angle value to the left (from 0 to 100).
        /// </summary>
        public byte Left { private set; get; }
        /// <summary>
        /// Return rotation angle value to the rigth (from 0 to 100).
        /// </summary>
        public byte Rigth { private set; get; }
        /// <summary>
        /// Return forward value (from 0 to 100).
        /// </summary>
        public byte Forward { private set; get; }
        /// <summary>
        /// Return backward value (from 0 to 100).
        /// </summary>
        public byte Backward { private set; get; }

        /// <summary>
        /// Event occured when user turn on left angle.
        /// </summary>
        public event JoistickValueDelegate JoistickLeftValueChangedEvent;
        /// <summary>
        /// Event occured when user turn on rigth.
        /// </summary>
        public event JoistickValueDelegate JoistickRigthValueChangedEvent;
        /// <summary>
        /// Event occured when user change value of forward.
        /// </summary>
        public event JoistickValueDelegate JoistickForwardValueChangedEvent;
        /// <summary>
        /// Event occured when user change value of backward.
        /// </summary>
        public event JoistickValueDelegate JoistickBackwardValueChangedEvent;

        /// <summary>
        /// Initialize a single instance of class JoistickControl.
        /// </summary>
        public JoistickControl()
        {
            // Initialize properties values
            this.Left = 0;
            this.Rigth = 0;
            this.Forward = 0;
            this.Backward = 0;
            // Initilize joistick device
            Guid _guid = new Guid();
            foreach (DeviceInstance _dev in this.di.GetDevices(DeviceType.Joystick, DeviceEnumerationFlags.AttachedOnly))
                _guid = _dev.ProductGuid;
            this.joist = new Joystick(this.di, _guid);
            this.joist.Properties.AxisMode = DeviceAxisMode.Absolute;
            this.joist.Acquire();
            // Start thread
            Task.Run(() =>
            {
                this.JoistickPoolThread();
            });
        }

        /// <summary>
        /// Thread body of joistick pool.
        /// </summary>
        private void JoistickPoolThread()
        {
            JoystickState _js = new JoystickState();
            while (!this.isNeedStop)
            {
                this.joist.GetCurrentState(ref _js);
                // X (Left || Rigth)
                if (_js.X > ZeroPoint) // To rigth
                    this.tRigth = (byte)(100 * (_js.X - ZeroPoint) / (MaxValuePoint - ZeroPoint));
                else if (_js.X < ZeroPoint) // To left
                    this.tLeft = (byte)(100 - (100 * _js.X / ZeroPoint));
                else
                    this.tLeft = this.tRigth = 0;
                // Y (Forward || Backward)
                if (_js.Y > ZeroPoint)
                    this.tBackward = (byte)(100 * (_js.Y - ZeroPoint) / (MaxValuePoint - ZeroPoint)); // Left pedal
                else if (_js.Y < ZeroPoint)
                    this.tForward = (byte)(100 - 100 * _js.Y / ZeroPoint); // Rigth pedal
                else
                    this.tForward = this.tBackward = 0;

                // If new left value not equal to last
                if (this.tLeft != this.Left)
                {
                    this.Left = this.tLeft; // Save new value
                    if (this.JoistickLeftValueChangedEvent != null) // Generate event
                        this.JoistickLeftValueChangedEvent(this, new JoistickEventArgs(this.tLeft));
                }
                // If new rigth value not equal to last
                if (this.tRigth != this.Rigth)
                {
                    this.Rigth = this.tRigth; // Save new value
                    if (this.JoistickRigthValueChangedEvent != null) // Generate event
                        this.JoistickRigthValueChangedEvent(this, new JoistickEventArgs(this.tRigth));
                }
                // If new speed not equal to last
                if (this.tForward != this.Forward)
                {
                    this.Forward = this.tForward; // Save new value
                    if (this.JoistickForwardValueChangedEvent != null) // Generate event
                        this.JoistickForwardValueChangedEvent(this, new JoistickEventArgs(this.tForward));
                }
                // If new speed not equal to last
                if (this.tBackward != this.Backward)
                {
                    this.Backward = this.tBackward; // Save new value
                    if (this.JoistickBackwardValueChangedEvent != null) // Generate event
                        this.JoistickBackwardValueChangedEvent(this, new JoistickEventArgs(this.tBackward));
                }
                Thread.Sleep(5);
            }

        }

    }
}
