﻿
namespace RpiGraphicClient.Control
{

    /// <summary>
    /// Present event properties, occuired when user change any value.
    /// </summary>
    class JoistickEventArgs : System.EventArgs
    {

        /// <summary>
        /// Return value of occuired event (from 0 to 100).
        /// </summary>
        public byte Value { private set; get; }

        /// <summary>
        /// Initialize a single instance of class JoistickEventArgs.
        /// </summary>
        /// <param name="val">Value (from 0 to 100).</param>
        public JoistickEventArgs(byte val)
        {
            this.Value = val;
        }

    }

}
