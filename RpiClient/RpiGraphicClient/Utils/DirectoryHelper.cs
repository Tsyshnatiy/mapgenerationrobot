﻿using System.IO;
using System.Reflection;

namespace RpiGraphicClient.Utils
{
    public static class DirectoryHelper
    {
        public static string GetProjectDir()
        {
            var dir = Directory.GetParent(Assembly.GetEntryAssembly().Location);
            dir = Directory.GetParent(dir.FullName);
            dir = Directory.GetParent(dir.FullName);

            return dir.FullName;
        }
    }
}
