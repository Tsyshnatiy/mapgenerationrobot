﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace RpiGraphicClient.Utils
{
    internal static class Logger
    {
        private static readonly string Filename = DirectoryHelper.GetProjectDir() + "/log.txt";

        public static void Log(string message)
        {
            using (var writer = new StreamWriter(Filename, true))
            {
                writer.WriteLine("LOG: " + message);
            }
        }

        public static void InitLogger()
        {
            if (File.Exists(Filename))
                File.Delete(Filename);
        }

        public static void LogHeaders(IDictionary<string, string> headers)
        {
            using (var writer = new StreamWriter(Filename, true))
            {
                writer.WriteLine("LOG: got incoming headers: ");
                foreach (var header in headers)
                {
                    writer.WriteLine(header.Key + ": " + header.Value);
                }
            }
        }
    }
}
