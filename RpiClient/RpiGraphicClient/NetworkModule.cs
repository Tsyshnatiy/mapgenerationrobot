﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using RpiGraphicClient.EventArgs;
using RpiGraphicClient.Exceptions;

namespace RpiGraphicClient
{
    internal class NetworkModule : IDisposable
    {
        private TcpClient _client;
        private NetworkStream _clientStrm;
        private readonly string _ipAddr;
        private readonly int _port;
        private bool _isDisposed;
        private bool _isConnected;
        private int _requestId;

        public event EventHandler<IncomingHeadersEventArgs> OnIncomingHeaders;

        public NetworkModule(string ip, int port)
        {
            _ipAddr = ip;
            _port = port;
        }

        private int Read(byte[] buffer, int offset, int count)
        {
            if (!_isConnected)
                throw new Exception("NetworkModule is not connected to any endpoint");

            return _clientStrm.Read(buffer, offset, count);
        }

        private void Write(byte[] buffer, int offset, int count)
        {
            if (!_isConnected)
                throw new Exception("NetworkModule is not connected to any endpoint");

            _clientStrm.Write(buffer, offset, count);
        }

        private string GetIdHeader(int id)
        {
            return HeaderNames.Id + ": " + id.ToString() + "\r\n";
        }

        private Dictionary<string, string> ReadHeaders()
        {
            if (!_isConnected)
                throw new Exception("NetworkModule is not connected to any endpoint");
            var headers = new Dictionary<string, string>();

            var lineBuffer = new byte[1024];
            string header = String.Empty;
            int totalBytesCame = 0;
            int bytesOfLastHeader = 0;

            while (true)
            {
                bool gotException = false;
                var bf = new byte[1];
                int bytesCame = Read(bf, 0, 1);

                if (bytesCame == 0)
                    throw new ConnectionLostException(_ipAddr, _port);

                Buffer.BlockCopy(bf, 0, lineBuffer, totalBytesCame, bytesCame);
                totalBytesCame += bytesCame;
                try
                {
                    header = Encoding.UTF8.GetString(lineBuffer, bytesOfLastHeader,
                                                        totalBytesCame - bytesOfLastHeader);
                }
                catch
                {
                    gotException = true;
                }

                if (totalBytesCame != 0 && !gotException && header[header.Length - 1] == '\n')
                {
                    header = header.TrimEnd('\n', '\r');

                    // empty header means we got \r\n\r\n which was trimmed. This means end of headers block.
                    if (String.IsNullOrEmpty(header))
                    {
                        break;
                    }

                    var nameValue = header.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);

                    if (nameValue.Length != 2)
                        throw new Exception("Incorrect header format");

                    headers.Add(nameValue[0], nameValue[1]);
                    bytesOfLastHeader = totalBytesCame;
                }
            }
            return headers;
        }

        public void Connect()
        {
            _client = new TcpClient(_ipAddr, _port);
            _clientStrm = _client.GetStream();

            _isConnected = true;
        }

        public void Disconnect()
        {
            Dispose();
        }

        public void WriteHeaders(IDictionary<string, string> headers)
        {
            if (!_isConnected)
                throw new Exception("NetworkModule is not connected to any endpoint");

            var requestAssembler = new StringBuilder(GetIdHeader(_requestId++));

            foreach (var header in headers)
            {
                requestAssembler.Append(header.Key + ": " + header.Value + "\r\n");
            }
            requestAssembler.Append("\r\n"); //Add end headers block symbol

            var reqStr = requestAssembler.ToString();
            var reqBytes = Encoding.UTF8.GetBytes(reqStr);

            Write(reqBytes, 0, reqBytes.Length);
        }

        public void StartIncomingPump()
        {
            while (_isConnected) 
            {
                try
                {
                    var headers = ReadHeaders();

                    if (OnIncomingHeaders != null)
                        OnIncomingHeaders(this, new IncomingHeadersEventArgs(headers));
                }
                catch (ConnectionLostException)
                {
                    break;
                }
            }
        }

        public void Dispose()
        {
            if (_isDisposed)
                return;

            _clientStrm.Dispose();
            _clientStrm = null;

            _client = null; //does not impl IDisposable
            OnIncomingHeaders = null;
            _isDisposed = true;
            _isConnected = false;
        }
    }
}
