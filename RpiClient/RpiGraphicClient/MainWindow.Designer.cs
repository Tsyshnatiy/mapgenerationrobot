﻿namespace RpiGraphicClient
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ExitBtn = new System.Windows.Forms.Button();
            this.grpLiveView = new System.Windows.Forms.GroupBox();
            this.imgLiveView = new System.Windows.Forms.PictureBox();
            this.grpMap = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.grpControl = new System.Windows.Forms.GroupBox();
            this.pnlControl = new System.Windows.Forms.Panel();
            this.grpLiveView.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgLiveView)).BeginInit();
            this.grpMap.SuspendLayout();
            this.grpControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // ExitBtn
            // 
            this.ExitBtn.Location = new System.Drawing.Point(251, 19);
            this.ExitBtn.Name = "ExitBtn";
            this.ExitBtn.Size = new System.Drawing.Size(75, 23);
            this.ExitBtn.TabIndex = 4;
            this.ExitBtn.Text = "Exit";
            this.ExitBtn.UseVisualStyleBackColor = true;
            this.ExitBtn.Click += new System.EventHandler(this.ExitBtn_Click);
            // 
            // grpLiveView
            // 
            this.grpLiveView.Controls.Add(this.imgLiveView);
            this.grpLiveView.Location = new System.Drawing.Point(12, 12);
            this.grpLiveView.Name = "grpLiveView";
            this.grpLiveView.Size = new System.Drawing.Size(333, 267);
            this.grpLiveView.TabIndex = 6;
            this.grpLiveView.TabStop = false;
            this.grpLiveView.Text = "Live View";
            // 
            // imgLiveView
            // 
            this.imgLiveView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.imgLiveView.Location = new System.Drawing.Point(6, 19);
            this.imgLiveView.Name = "imgLiveView";
            this.imgLiveView.Size = new System.Drawing.Size(320, 240);
            this.imgLiveView.TabIndex = 0;
            this.imgLiveView.TabStop = false;
            // 
            // grpMap
            // 
            this.grpMap.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpMap.Controls.Add(this.panel1);
            this.grpMap.Location = new System.Drawing.Point(351, 12);
            this.grpMap.Name = "grpMap";
            this.grpMap.Size = new System.Drawing.Size(435, 538);
            this.grpMap.TabIndex = 7;
            this.grpMap.TabStop = false;
            this.grpMap.Text = "Map";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Location = new System.Drawing.Point(6, 19);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(423, 513);
            this.panel1.TabIndex = 6;
            // 
            // grpControl
            // 
            this.grpControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.grpControl.Controls.Add(this.ExitBtn);
            this.grpControl.Controls.Add(this.pnlControl);
            this.grpControl.Location = new System.Drawing.Point(12, 285);
            this.grpControl.Name = "grpControl";
            this.grpControl.Size = new System.Drawing.Size(333, 267);
            this.grpControl.TabIndex = 8;
            this.grpControl.TabStop = false;
            this.grpControl.Text = "Control";
            // 
            // pnlControl
            // 
            this.pnlControl.Location = new System.Drawing.Point(6, 19);
            this.pnlControl.Name = "pnlControl";
            this.pnlControl.Size = new System.Drawing.Size(200, 200);
            this.pnlControl.TabIndex = 0;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(798, 562);
            this.Controls.Add(this.grpControl);
            this.Controls.Add(this.grpMap);
            this.Controls.Add(this.grpLiveView);
            this.Name = "MainWindow";
            this.Text = "RpiClient";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainWindow_FormClosed);
            this.grpLiveView.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgLiveView)).EndInit();
            this.grpMap.ResumeLayout(false);
            this.grpControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ExitBtn;
        private System.Windows.Forms.GroupBox grpLiveView;
        private System.Windows.Forms.PictureBox imgLiveView;
        private System.Windows.Forms.GroupBox grpMap;
        private System.Windows.Forms.GroupBox grpControl;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlControl;
    }
}

