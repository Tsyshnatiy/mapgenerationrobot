﻿using System.Collections.Generic;

namespace RpiGraphicClient.EventArgs
{
    public class IncomingHeadersEventArgs : System.EventArgs
    {
        public IDictionary<string, string> Headers { get; private set; }

        public IncomingHeadersEventArgs(IDictionary<string, string> headers)
        {
            Headers = headers;
        }
    }
}
