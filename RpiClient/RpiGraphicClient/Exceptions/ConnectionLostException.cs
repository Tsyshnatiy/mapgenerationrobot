﻿using System;

namespace RpiGraphicClient.Exceptions
{
    public class ConnectionLostException : Exception
    {
        public ConnectionLostException(string host, int port)
            : base(String.Format("Connection with {0} : {1} is lost",host, port))
        {
            
        }
    }
}
