#include "ImageContoursAnalyzer.h"

int ImageContoursAnalyzer::Process(IplImage*& image, CvRect* rect)
{
	//TODO do not reconstruct memStorage
	CvMemStorage* storage = cvCreateMemStorage(0);
    CvSeq* contour = NULL;
	cvFindContours(image, storage, &contour, sizeof(CvContour), CV_RETR_LIST , CV_CHAIN_APPROX_SIMPLE, cvPoint(0,0));
	for( ; contour != 0; contour = contour->h_next )
	{
		double actual_area = fabs(cvContourArea(contour, CV_WHOLE_SEQ, 0));
		if (actual_area < minArea)
			continue;

		*rect = cvBoundingRect(contour);
		break;
	}
	cvReleaseMemStorage(&storage);

	return 0;
}
