#include "GlobalRpiManager.h"

struct StartServerArgs
{
    networkCallback callback;
    NetworkModule* module;
    GlobalRpiManager* manager;
};

int GlobalRpiManager::processHeader(const string& name, const string& value, GlobalRpiManager* manager)
{
    MotorsController* mc = manager->mc;
    if(name == allowedHeaders::forward)
    {
        if (value == "true")
        {
            mc->Forward();
        }
        else
        {
            return RPI_INCORRECT_COMMAND;
        }
    }
    else if(name == allowedHeaders::backward && value == "true")
    {
        if (value == "true")
        {
            mc->Backward();
        }
        else
        {
            return RPI_INCORRECT_COMMAND;
        }
    }
    else if(name == allowedHeaders::left && value == "true")
    {
        if (value == "true")
        {
            mc->Left();
        }
        else
        {
            return RPI_INCORRECT_COMMAND;
        }
    }
    else if(name == allowedHeaders::right && value == "true")
    {
        if (value == "true")
        {
            mc->Right();
        }
        else
        {
            return RPI_INCORRECT_COMMAND;
        }
    }
    else if(name == allowedHeaders::stop && value == "true")
    {
        if (value == "true")
        {
            mc->Stop();
        }
        else
        {
            return RPI_INCORRECT_COMMAND;
        }
    }

    Logger::LogConsole("GlobalRpiManager processed header");
    Logger::LogConsole(name + ": " + value);

    return RPI_OK;
}

int GlobalRpiManager::networkHandler(const HeadersBlock* headersBlock, void* args)
{
    StartServerArgs* startSrvArgs = reinterpret_cast<StartServerArgs*>(args);
    GlobalRpiManager* manager = startSrvArgs->manager;

    Logger::LogConsole("GlobalRpiManager called headers handler");
    map<string,string> headers = headersBlock->GetHeaders();
    map<string,string>::iterator it = headers.begin();

    for(; it != headers.end(); it++)
    {
        int errCode = processHeader(it->first, it->second, manager);

        if(errCode != RPI_OK)
            return errCode;
    }

    Logger::LogConsole("GlobalRpiManager finished headers handler");

    return RPI_OK;
}

GlobalRpiManager::GlobalRpiManager()
{
    serverThread = new pthread_t();
    nm = new NetworkModule();
    mc = new MotorsController();

    callback = &GlobalRpiManager::networkHandler;

    speed = 128;    //TODO check this value
    mvState = stop;

    Logger::LogConsole("GlobalRpiManager created");
}

void* GlobalRpiManager::startNetwork(void* args)
{
    StartServerArgs* startSrvArgs = reinterpret_cast<StartServerArgs*>(args);
    NetworkModule* nm = startSrvArgs->module;
    networkCallback callback = startSrvArgs->callback;

    return nm->Start(callback, args);
}

int GlobalRpiManager::Start()
{
    StartServerArgs args;
    args.callback = callback;
    args.module = nm;
    args.manager = this;

    pthread_create(serverThread, NULL, GlobalRpiManager::startNetwork, &args);

    Logger::LogConsole("serverThread started");

    //TODO remove it
    //pthread_join(*serverThread, NULL);
    //Logger::LogConsole("ServerThread joined");

    return RPI_OK;
}

GlobalRpiManager::~GlobalRpiManager()
{
    //TODO uncomment this
    pthread_join(*serverThread, NULL);

    Logger::LogConsole("ServerThread joined");

    delete(serverThread);
    delete(nm);
    delete(mc);

    Logger::LogConsole("GlobalRpiManager destructor finished");
}


