#include "Headers.h"

HeadersBlock::HeadersBlock()
{
    headers = new map<string,string>();
}

HeadersBlock::HeadersBlock(const HeadersBlock *block)
{
    headers = new map<string,string>(*(block->headers));
}

int HeadersBlock::Add(pair<string,string> h)
{
    if (headers->find(h.first) != headers->end())
            return RPI_KEY_EXISTS;

    headers->insert(h);
    return RPI_OK;
}

int HeadersBlock::Remove(const string& name)
{
    if (headers->find(name) == headers->end())
            return RPI_NOT_FOUND;

    headers->erase(name);
    return RPI_OK;
}

int HeadersBlock::GetValue(const string& name, string& result)
{
    map<string,string>::const_iterator pos = headers->find(name);

    if (pos == headers->end())
            return RPI_NOT_FOUND;

    result = (*headers)[name];
    return RPI_OK;
}

bool HeadersBlock::Contains(const string& name)
{
    return headers->find(name) != headers->end();
}

bool HeadersBlock::Contains(const string& name, const string& value)
{
    map<string,string>::iterator it = headers->begin();
    for(; it != headers->end() ; it++)
    {
        if (it->first == name && it->second == value)
            return true;
    }

    return false;
}

HeadersBlock::~HeadersBlock()
{
    delete headers;
    Logger::LogConsole("HeadersBlock destructor finished");
}


