#include "ImageFilterProcessor.h"

IplImage* ImageFilterProc::Crop(IplImage* image, const CvRect* roi)
{
	cvSetImageROI(image, *roi);

	IplImage* cropped = cvCreateImage(cvGetSize(image), image->depth, image->nChannels);
		
	cvCopy(image, cropped, NULL);
	
	return cropped;
}

int ImageFilterProc::Process(IplImage*& image, CvRect* roi)
{
	image = Crop(image, roi);

    cvThreshold( image, image, 250, 255, CV_THRESH_BINARY);
    //cvXorS(src, cvScalar(255, 0, 0, 0), src, NULL);
    //cvDilate(image, image, NULL, 2); 
	return 0;
}
