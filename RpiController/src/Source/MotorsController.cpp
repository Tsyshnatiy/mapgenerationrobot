#include "MotorsController.h"
#include <wiringPi.h>
#include "stdlib.h"

MotorsController::MotorsController()
{
    if (wiringPiSetup() == -1)
    {
        Logger::LogConsole("wiring pi cant start up");
        exit(1);
    }

    speed = 730;

    pinMode(motorLFw, OUTPUT);
    pinMode(motorLBk, OUTPUT);
    pinMode(motorRFw, OUTPUT);
    pinMode(motorRBk, OUTPUT);
    pinMode(motorSpdReg, PWM_OUTPUT);

    PowerOn();
}

void MotorsController::PowerOff()
{
    digitalWrite(motorLFw, LOW);
    digitalWrite(motorLBk, LOW);
    digitalWrite(motorRFw, LOW);
    digitalWrite(motorRBk, LOW);

    pwmWrite(motorSpdReg, 0);

    delay(25);
}

void MotorsController::PowerOn()
{
    pwmWrite(motorSpdReg, speed);

    delay(25);
}

int MotorsController::Forward()
{
    PowerOff();
    PowerOn();

    digitalWrite(motorLFw, HIGH);
    digitalWrite(motorRFw, HIGH);

    Logger::LogConsole("Moving forward");

    return RPI_OK;
}

int MotorsController::Backward()
{
    PowerOff();
    PowerOn();

    digitalWrite(motorLBk, HIGH);
    digitalWrite(motorRBk, HIGH);

    Logger::LogConsole("Moving left");

    return RPI_OK;
}

int MotorsController::Left()
{
    PowerOff();
    PowerOn();

    digitalWrite(motorLBk, HIGH);
    digitalWrite(motorRFw, HIGH);

    Logger::LogConsole("Turning left");

    return RPI_OK;
}

int MotorsController::Right()
{
    PowerOff();
    PowerOn();

    digitalWrite(motorLFw, HIGH);
    digitalWrite(motorRBk, HIGH);

    Logger::LogConsole("Turning right");

    return RPI_OK;
}

int MotorsController::Stop()
{
    PowerOff();

    return RPI_OK;
}

MotorsController::~MotorsController()
{
    PowerOff();
}

