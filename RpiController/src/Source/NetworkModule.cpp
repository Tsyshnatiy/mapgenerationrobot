#include "NetworkModule.h"
#include <string.h>
#include <string>
#include "Utils.h"

using namespace utils;
using namespace std;

NetworkModule::NetworkModule()
{
    bufferSize = 128;
    buffer = (char*) malloc(sizeof(byte) * bufferSize);
    listener = socket(AF_INET, SOCK_STREAM, 0);
    if(listener < 0)
    {
        Logger::LogConsole("NetworkModule: cant create listener", RPI_INTERNAL_ERROR);
        return;
    }

    addr.sin_family = AF_INET;
    addr.sin_port = htons(5051);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    if(bind(listener, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        Logger::LogConsole("NetworkModule: cant bind listener", RPI_INTERNAL_ERROR);
        return;
    }

    listen(listener, 1);

    sock = accept(listener, NULL, NULL);
    if(sock < 0)
    {
        Logger::LogConsole("NetworkModule: cant accept client", RPI_INTERNAL_ERROR);
        return;
    }

    isConnLost = false;
    headersDelim = "\r\n";
    Logger::LogConsole("NetworkModule: server started");
}

void NetworkModule::Write(const byte* bytes)
{
    send(sock, bytes, strlen(bytes), 0);
    Logger::LogConsole("NetworkModule: package sent");
}

int NetworkModule::Read(byte* buffer, int len)
{
    Logger::LogConsole("NetworkModule: incoming message");
    return recv(sock, buffer, len, 0);
}

void NetworkModule::Stop()
{
    Logger::LogConsole("NetworkModule Stop called");
}

void* NetworkModule::Start(networkCallback callback, void* args)
{
    int errCode = RPI_OK;

    while(true)
    {
        int bytes_read = this->Read(buffer, bufferSize);

        if(bytes_read <= 0)
        {
            Logger::LogConsole("Connection lost");
            isConnLost = true;
            break;
        }

        byte* package = (byte*) malloc(sizeof(byte) * bytes_read);
        memcpy(package, buffer, bytes_read);

        HeadersBlock* headers = new HeadersBlock();

        errCode = ParseHeaders(&headers, package);

        if (errCode != RPI_OK)
        {
            Logger::LogConsole("parse headers error", errCode);

            //callback should process package and copy it if necessary
            free(package);
            delete(headers);

            break;
        }

        string idStr;
        if (headers->GetValue(allowedHeaders::id, idStr) == RPI_NOT_FOUND)
        {
            WriteStatus(-1, 400); //return bad request
            continue;
        }

        int id = utils::FromString(idStr);

        if (callback)
        {
            errCode = callback(headers, args);

            if (errCode != RPI_OK)
            {
                Logger::LogConsole("network callback error", errCode);

                WriteStatus(id,500);

                //callback should process package and copy it if necessary
                free(package);
                delete(headers);

                break;
            }
        }

        WriteStatus(id,200);

        string exit;
        if (headers->GetValue(allowedHeaders::exit, exit) != RPI_NOT_FOUND)
            break;

        //callback should process package and copy it if necessary
        free(package);
        delete(headers);
    }

    return NULL;
}

void NetworkModule::WriteStatus(const int id, const int status)
{
    string idStr = utils::toString<int>(id);
    string statusStr = utils::toString<int>(status);
    string response = allowedHeaders::id
                        + ": "
                        + idStr
                        + "\r\n"
                        + allowedHeaders::status
                        + ": "
                        + statusStr
                        + "\r\n\r\n";

    this->Write(response.c_str());
}

int NetworkModule::ParseHeaders(HeadersBlock** result, const byte* bytes)
{
    int curIndex = 0;
    string package(bytes);

    if(*result == NULL)
    {
        *result = new HeadersBlock();
    }

    while(true)
    {
        int hend = package.find(headersDelim, curIndex);

        if (hend == -1)
        {
            goto parse_error;
        }

        if (curIndex == hend && package[curIndex] == '\r')
            break;

        string headerStr = package.substr(curIndex, hend - curIndex);

        curIndex = hend + headersDelim.size();    //update search index

        int colonPos = headerStr.find(':');

        if (colonPos == -1)
        {
            goto parse_error;
        }

        string name = headerStr.substr(0, colonPos);
        string value = headerStr.substr(colonPos + 1, headerStr.size() - 1 - colonPos);

        //always returns RPI_OK
        if ((utils::Trim(name) & utils::Trim(value)) != RPI_OK)
        {
            goto parse_error;
        }

        pair<string,string> header(name,value);

        (*result)->Add(header);
    }

    return RPI_OK;

parse_error:
    Logger::LogConsole("networkModule: invalid headers", RPI_INTERNAL_ERROR);
    return RPI_INTERNAL_ERROR;
}

NetworkModule::~NetworkModule()
{
    Stop();
    free(buffer);
    buffer = NULL;

    Logger::LogConsole("NetworkModule destructor finished");
}
