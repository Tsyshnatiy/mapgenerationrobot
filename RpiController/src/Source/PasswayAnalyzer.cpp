#include "PasswayAnalyzer.h"

tuple<float,float,float> PasswayAnalyzer::getLineEquation(const CvPoint2D32f& pt1, const CvPoint2D32f& pt2)
{
	return make_tuple(pt2.y - pt1.y, pt1.x - pt2.x, pt1.x * (pt1.y - pt2.y) + pt1.y * (pt2.x - pt1.x));
}

int PasswayAnalyzer::findPrevOppositeSign(const vector<byte>& flags, int pos, bool value)
{
    byte pattern = value ? 1 : -1;
    for (int i = pos; i >= 0 ; i--)
	{
		if (!flags[i])
			continue;

		if (flags[i] * pattern < 0)
			return i;

		return -1;
	}

	return -1;
}

vector<Passway> PasswayAnalyzer::Analyze(const vector<CvPoint2D32f>& distances)
{
    vector<Passway> res;
    vector<byte> flags(distances.size());
	for (vector<CvPoint>::size_type i = 0 ; i < distances.size() - 1 ; i++)
	{
		tuple<float,float,float> equation = getLineEquation(distances[i], distances[i+1]);

		//ax + by + c = 0
		float a = get<0>(equation);
        float b = get<1>(equation);

		if (!(a && b)) //vertical line - impossible in our terms/ horizontal line - possible, but non-informative
			continue;

		float k = -a / b; //y = kx + d;
		printf("k = %f; i = %d\n",k,i);
		if (fabs(k) >= limitTan) 
		{
			flags[i] = k > 0 ? 1 : -1;
			if (!i)
				continue;

			int oppositeSignIndex = findPrevOppositeSign(flags, i - 1, flags[i]);
			if (oppositeSignIndex == -1)
			{
				//this means 000010000001 for example
				continue;
			}
			
			//this means 00001000010000-1
			flags[i] = 0;
			flags[oppositeSignIndex] = 0; //000010000000000
			res.push_back(Passway(oppositeSignIndex, i));
		}
	}

	return res;
}
