#include "ImagePostProcessor.h"

int ImagePostProcessor::drawCross(IplImage* image, const CvPoint& center, const CvScalar& color, const int thickness)
{
	CvPoint lxb = {0, center.y};
	CvPoint lxe = {SCR_RES_WID, center.y};
				
	CvPoint lyb = {center.x, 0};
	CvPoint lye = {center.x, SCR_RES_HEI};

	cvLine(image, lxb, lxe, color, thickness);
	cvLine(image, lyb, lye, color, thickness);

	return 0;
}

int ImagePostProcessor::Process(IplImage*& image, CvRect* boundRect)
{
	CvPoint imageCntr = {SCR_RES_WID / 2, SCR_RES_HEI / 2};
	CvPoint rectCntr = {boundRect->width / 2 + boundRect->x, boundRect->height / 2 + boundRect->y};

	drawCross(image, imageCntr, green, 1);
	drawCross(image, rectCntr, blue, 1);

	return 0;
}
