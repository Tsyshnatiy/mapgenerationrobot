#include "Logger.h"
#include <iostream>

using namespace std;

void Logger::LogConsole(const char* message, const int errorCode)
{
    printf("LOG_CONSOLE: %s\r\n", message);

    if (errorCode != RPI_OK)
        printf("LOG_CONSOLE: LAST_ERROR = %d\r\n", errorCode);
}

void Logger::LogConsole(const string& message, const int errorCode)
{
    cout << "LOG_CONSOLE: " << message << endl;

    if (errorCode != RPI_OK)
        cout << "LOG_CONSOLE: LAST_ERROR = " << errorCode << endl;
}

void Logger::LogPackage(const byte* package)
{
    printf("LOG_CONSOLE: package dump = %s\r\n", package);
}
