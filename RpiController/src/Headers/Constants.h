#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <string>

using namespace std;

#define RPI_OK 1
#define RPI_NET_ERROR 2
#define RPI_INTERNAL_ERROR 3
#define RPI_NOT_FOUND 4
#define RPI_KEY_EXISTS 5
#define RPI_INCORRECT_COMMAND 6
#define RPI_UNKNOWN_ERROR 255
#define SCR_RES_WID 320
#define SCR_RES_HEI 240

typedef char byte;

struct Passway
{
    int begin;
    int end;

    Passway(int b, int e)
    {
        begin = b;
        end = e;
    }
};

namespace allowedHeaders
{
    static const string id = "id";
    static const string speed = "speed";
    static const string exit = "exit";
    static const string status = "status";
    static const string forward = "forward";
    static const string backward = "backward";
    static const string right = "right";
    static const string left = "left";
    static const string stop = "stop";
    static const string manual = "manual";
}

#endif // CONSTANTS_H
