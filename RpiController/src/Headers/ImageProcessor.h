#ifndef IMAGE_PROC_H
#define IMAGE_PROC_H

#include "Constants.h"
#include "opencv/cv.h"

class ImageProcessor
{
public:
    virtual int Process(IplImage*&, CvRect*) = 0;
};

#endif //IMAGE_PROC_H
