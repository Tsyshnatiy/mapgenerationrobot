#ifndef PASSWAY_ANALYZER
#define PASSWAY_ANALYZER

#include "Constants.h"
#include <vector>
#include "opencv/cv.h"
#include <tuple>

using namespace std;

class PasswayAnalyzer
{
private:
	const float limitTan;
	tuple<float,float,float> getLineEquation(const CvPoint2D32f& pt1, const CvPoint2D32f& pt2);

	//value is true if need to find positive
    int findPrevOppositeSign(const vector<byte>& flags, int pos, bool value);
public:
	PasswayAnalyzer(): limitTan(11.43f) //this is tan(85)
	{
	}

	vector<Passway> Analyze(const vector<CvPoint2D32f>& distances);
};

#endif //PASSWAY_ANALYZER
