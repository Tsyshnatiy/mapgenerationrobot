#ifndef IMAGE_POST_PROC_H
#define IMAGE_POST_PROC_H

#include "Constants.h"
#include "opencv/cv.h"
#include "ImageProcessor.h"

class ImagePostProcessor : public ImageProcessor
{
private:
    int drawCross(IplImage* image, const CvPoint& center, const CvScalar& color, const int thickness = 2);
	const CvScalar red;
	const CvScalar green;
	const CvScalar blue;
public:
	ImagePostProcessor(): red(CV_RGB(0, 0, 255)), green(CV_RGB(0, 255, 0)), blue(CV_RGB(255, 0, 0))
	{
	}

    int Process(IplImage*& image, CvRect* boundRect);
};

#endif //IMAGE_POST_PROC_H
