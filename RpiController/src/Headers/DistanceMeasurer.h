#ifndef DIST_MEAS_H
#define DIST_MEAS_H

#include "Constants.h"
#include "opencv/cv.h"

class DistanceMeasurer
{
private:
	const double k;
	const double b;
public:
	//640*480 : k = 1237.48453081; b = 2.37898898048
	//320*240 : k = 638.359904279; b = 1.95812898409
	DistanceMeasurer(): k(663.936391544), b(2.04595656268)
	{
	}

	float GetDistance(const CvPoint& center, const CvPoint& boundCenter) const;
};

#endif //DIST_MEAS_H
