#ifndef IMAGE_PROC_FILTER_H
#define IMAGE_PROC_FILTER_H

#include "Constants.h"
#include "ImageProcessor.h"
#include "opencv/cv.h"

class ImageFilterProc : public ImageProcessor
{
private:
	IplImage* Crop(IplImage* image, const CvRect* roi);
public:
    int Process(IplImage*& image, CvRect* roi);
};

#endif //IMAGE_PROC_FILTER_H
