#ifndef IMAG_CONTR_ANALYZ
#define IMAG_CONTR_ANALYZ

#include "opencv/cv.h"
#include "ImageProcessor.h"
#include "Constants.h"

class ImageContoursAnalyzer : public ImageProcessor
{
private:
	const int minArea;
public:
	ImageContoursAnalyzer() : minArea(3)
	{
	}

    int Process(IplImage*& image, CvRect* rect);
};

#endif //IMAG_CONTR_ANALYZ
