#ifndef MOTORSCONTROLLER_H
#define MOTORSCONTROLLER_H

#include "Logger.h"

enum MovementState
{
    forward = 0,
    backward = 1,
    left = 2,
    right = 3,
    stop = 4
};

class MotorsController
{
private:
    //TODO declare encoder pins
    static const int motorLFw = 5;
    static const int motorLBk = 4;
    static const int motorRFw = 3;
    static const int motorRBk = 2;
    static const int motorSpdReg = 1;

    int speed;

    void PowerOff();
    void PowerOn();
public:
    MotorsController();

    int Forward();
    int Backward();
    int Left();
    int Right();
    int Stop();

    void SetSpeed(const int speed) { this->speed = speed; }

    ~MotorsController();
};

#endif // MOTORSCONTROLLER_H
