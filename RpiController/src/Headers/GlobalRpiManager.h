#ifndef GLOBALRPIMANAGER_H
#define GLOBALRPIMANAGER_H

#include <pthread.h>
#include "Constants.h"
#include "NetworkModule.h"
#include "Logger.h"
#include "MotorsController.h"

class GlobalRpiManager;
typedef int (*networkCallback)(const HeadersBlock* headers, void* args);

//TODO make it singletone
class GlobalRpiManager
{
private:
    int volatile speed;    //TODO check this value
    MovementState mvState;

    pthread_t* serverThread;
    NetworkModule* nm;
    MotorsController* mc;

    networkCallback callback;
    static void* startNetwork(void*);
    static int networkHandler(const HeadersBlock* headersBlock, void* args);
    static int processHeader(const string& name, const string& value, GlobalRpiManager* manager);
public:
    GlobalRpiManager();

    int Start();

    bool IsTimeToExit() {return nm->IsConnectionLost();}

    ~GlobalRpiManager();
};

#endif // GLOBALRPIMANAGER_H
