#ifndef HEADER_H
#define HEADER_H

#include "Constants.h"
#include "Logger.h"
#include <map>
#include <string>

using namespace std;

//stores unique headers
class HeadersBlock
{
private:
    map<string,string>* headers;
public:
    HeadersBlock();
    HeadersBlock(const HeadersBlock* block);

    int Add(pair<string,string> h);                                 //pass by value and copy header.
    int Remove(const string& name);
    int GetValue(const string& name, string& result);               //returns value
    bool Contains(const string& name);
    bool Contains(const string& name, const string& value);

    map<string,string>& GetHeaders() const { return *headers; }
    ~HeadersBlock();
};

#endif // HEADER_H
