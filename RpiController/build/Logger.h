#ifndef LOGGER_H
#define LOGGER_H

#include <stdio.h>
#include "Constants.h"

class Logger
{
public:
    static void LogConsole(const char* message, const int errorCode = RPI_OK);

    static void LogConsole(const string& message, const int errorCode = RPI_OK);

    static void LogPackage(const byte* package);
};

#endif // LOGGER_H
