#ifndef UTILS_H
#define UTILS_H

#include <string>
#include <sstream>
#include "Constants.h"

using namespace std;

namespace utils
{
    static int TrimBegin(string& str)
    {
        int pos = str.find_first_not_of(' ');

        str.erase(0, pos);
        return RPI_OK;
    }

    static int TrimEnd(string& str)
    {
        int pos = str.find_last_not_of(' ');

        str.erase(pos + 1);
        return RPI_OK;
    }

    static int Trim(string& str)
    {
        return TrimBegin(str) & TrimEnd(str);
    }

    template<typename T>
    string toString(const T& value)
    {
        std::ostringstream oss;
        oss << value;
        return oss.str();
    }

    int FromString(const string& str)
    {
        int value;
        istringstream sstream(str);
        sstream >> value;

        return value;
    }
}
#endif // UTILS_H
