#ifndef NETWORKMODULE_H
#define NETWORKMODULE_H

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include "Constants.h"
#include "Logger.h"
#include "Headers.h"
#include <string>

using namespace std;

class GlobalRpiManager;
typedef int (*networkCallback)(const HeadersBlock* headers, void* args);

//TODO make it singleton
class NetworkModule
{
private:
    int sock, listener;
    struct sockaddr_in addr;
    byte* buffer;
    uint8_t bufferSize;
    string headersDelim;
    bool isConnLost;
    void WriteStatus(const int id, const int status);
public:
    NetworkModule();

    void* Start(networkCallback, void*);

    void Write(const byte* bytes);

    int Read(byte*, int len);

    void Stop();

    int ParseHeaders(HeadersBlock** result, const byte* bytes);

    bool IsConnectionLost() { return isConnLost;}

    ~NetworkModule();
};

#endif // NETWORKMODULE_H
